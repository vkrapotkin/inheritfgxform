unit MainForm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, FMX.Layouts, FMX.ExtCtrls, FMX.Controls.Presentation,
  FMX.ScrollBox, FMX.Memo, FMX.Edit, FMX.StdCtrls, FMX.TabControl;

type
  TForm1 = class(TForm)
    drop1: TDropTarget;
    m1: TMemo;
    mDpr: TMemo;
    lay1: TLayout;
    lay3: TLayout;
    lbl1: TLabel;
    eClassname: TEdit;
    lv1: TListView;
    btnPreview: TButton;
    eFilename: TEdit;
    lbl2: TLabel;
    lbl3: TLabel;
    tbc1: TTabControl;
    tabDpr: TTabItem;
    tabXfm: TTabItem;
    tabPas: TTabItem;
    tbc2: TTabControl;
    tabSelectForm: TTabItem;
    tabSelectClassname: TTabItem;
    lay2: TLayout;
    btnNext: TButton;
    lay4: TLayout;
    btnBack: TButton;
    lbl4: TLabel;
    mXfm: TMemo;
    mPas: TMemo;
    spl1: TSplitter;
    dlgOpen1: TOpenDialog;
    lblXfmAlreadyExists: TLabel;
    lblPasAlreadyExists: TLabel;
    btnSave: TButton;
    procedure drop1Dropped(Sender: TObject; const Data: TDragObject;
      const Point: TPointF);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnPreviewClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure drop1Click(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure eClassnameChangeTracking(Sender: TObject);
  private
    procedure LoadProject(prName: string);
    procedure CreateClassAndFilename;
    procedure CreatePasCode;
    procedure CreateXfmCode;
  public

    SelectedFormIndex:integer;
    basePath:string;
    SourceProject: string;
    SourceClassName : string;
    SourceModuleName: string;
    ResultModuleName:string;
    ResultRelPath:string;
    FormFiles:TStringList;
    FName:string;
    FList:TStringList;
    FgxAppIndex : integer;
    FInsertIndex:integer;
  end;

var
  Form1: TForm1;

implementation

uses
  System.IOUtils;

{$R *.fmx}


procedure TForm1.FormCreate(Sender: TObject);
begin
  FList := TStringList.Create;
  FormFiles := TStringList.Create;
  tbc2.TabPosition := TTabPosition.None;
  tbc2.ActiveTab := tabSelectForm;
  tbc1.ActiveTab := tabDpr;
  lblXfmAlreadyExists.Visible := False;
  lblPasAlreadyExists.Visible := False;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FList);
  FreeAndNil(FormFiles);
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  m1.Width := tbc1.Width *0.4 ;
end;

function GetFilename(s:string):string;
var
  p1,p2:integer;
begin
  result := '';
  p1 := pos('''', s);
  if p1<>0 then
  begin
    p2 := Pos('''', s, p1+1);
    if p2<>0 then
    begin
      result := Copy(s, p1+1, p2-p1-1);
    end;
  end;
end;

procedure TForm1.LoadProject(prName:string);
var
  s:string;
  i: Integer;
  fn: string;
  L:TStringList;
  p:integer;
  li:TListViewItem;
begin
  Flist.LoadFromFile(prname);
  BasePath := ExtractFilePath(prname);
  m1.Lines.Assign(flist);
  i :=0;
  while i < Flist.Count do
  begin
    s := Trim(Flist[i]);
    while pos('  ',s)<>0 do
      s := s.Replace('  ',' ',[rfReplaceAll]);
    FList[i]:=s;
    inc(i);
  end;

  FgxAppIndex := FList.IndexOf('FGX.Application,');
  if FgxAppIndex<0 then
    raise Exception.Create('��� �� FGX ������');

  SourceProject := prname;
  FormFiles.Clear;
  for i := FgxAppIndex+1 to FList.count-1 do
  begin
    if FList[i].Contains(' in ') then
    begin
      s:=getFilename(FList[i]);
      fn := ChangeFileExt(BasePath+s,'.xfm');
      if FileExists(fn) then
        FormFiles.Add(fn);
    end;
    if flist[i].EndsWith(';') then
    begin
      FInsertIndex := i;
      break;
    end;
  end;

  lv1.Items.Clear;
  for i := 0 to FormFiles.Count-1 do
  begin
    L:=TStringList.Create;
    l.LoadFromFile(FormFiles[i]);
    s:=l[0];
    p := Pos(':', s);
    if p=0 then
      raise Exception.Create('������ 13048: '+FormFiles[i]);

    s:= trim( s.Substring(p) );
    li := lv1.Items.Add;
    li.Text := s;
  end;

end;

procedure TForm1.btnBackClick(Sender: TObject);
begin
  tbc2.Previous();
end;

procedure TForm1.btnNextClick(Sender: TObject);
begin
  if lv1.Selected = nil then
    raise exception.Create('�������� ���� �� ������');
  tbc2.Next();
  CreateClassAndFilename();
end;

procedure TForm1.CreateClassAndFilename();
var
  cl, ext, moduleName :string;
begin
  if lv1.Selected=nil then
    exit;

  SourceClassname := (lv1.Selected as TListViewItem).Text;
  cl := SourceClassname + '1';
  if cl.StartsWith('T') then
    cl := cl.Substring(1);

  eClassname.Text := 'T'+cl;
  SelectedFormIndex := lv1.Selected.Index;

  sourcemodulename := ExtractFileName(FormFiles[SelectedFormIndex]);
  ext := ExtractFileExt( sourcemodulename );
  sourcemodulename := sourcemodulename.Substring(0, sourcemodulename.Length - ext.Length);
  moduleName := sourcemodulename;
  if modulename.StartsWith('U') then
    modulename := modulename.Substring(1);
  modulename := 'U'+cl;
  eFilename.Text := ExtractFilePath(FormFiles[SelectedFormIndex]).Substring(Length(basePath))+
    modulename +'.pas';
end;

procedure TForm1.btnPreviewClick(Sender: TObject);
var
  newline:string;
  ext:string;
  s:string;
begin
  ResultModuleName := ExtractFileName(basePath + eFilename.Text);
  ResultRelPath := ExtractFilePath(basePath + eFilename.Text);
  ResultRelPath := ResultRelPath.Substring(basepath.Length, ResultRelPath.Length - basepath.Length);
  ext := ExtractFileExt(ResultModuleName);
  ResultModuleName := ResultModuleName.Substring(0, ResultModuleName.Length - ext.Length);

  mDpr.Lines.Assign(m1.Lines);
//  Uso2SalePlacesForm in 'Uso2SalePlacesForm.pas' {so2SalePlacesForm: TfgForm},
  newLine := Format('  %s in ''%s'' {%s: TfgForm},',[ResultModuleName,
    ResultRelPath + ResultModuleName +'.pas',
    ResultModuleName.Substring(1)]);
  mdpr.Lines.Insert(FInsertIndex, newline);
  mdpr.SelStart := mdpr.Lines.Text.IndexOf(newLine);
  mdpr.SelLength := newline.Length;

  CreateXfmCode();
  CreatePasCode();

  lblXfmAlreadyExists.Visible := FileExists(basePath + ResultRelPath + ResultModuleName + '.xfm');
  lblPasAlreadyExists.Visible := FileExists(basePath + ResultRelPath + ResultModuleName + '.pas');
end;


procedure TForm1.btnSaveClick(Sender: TObject);
begin
  mDpr.Lines.SaveToFile(SourceProject);
  m1.Lines.Assign(mDpr.Lines);
  mXfm.Lines.SaveToFile(basePath + ResultRelPath + ResultModuleName + '.xfm');
  mPas.Lines.SaveToFile(basePath + ResultRelPath + ResultModuleName + '.pas');
  ShowMessage('������');
end;

procedure TForm1.CreateXfmCode();
var
  clname, varname:string;
begin
  clName := eClassName.Text;
  if clname.StartsWith('T') then
    varname := clname.Substring(1)
  else
    varname := 'frm'+clname;

  mXfm.lines.Clear;
  mXfm.Lines.Add(Format('inherited %s: %s',[varname, clname]));
  mXfm.Lines.Add('end');
end;

procedure TForm1.CreatePasCode();
begin
  mPas.Lines.Clear;
  mPas.Lines.Add(format('unit %s;',[ResultModuleName]));
  mPas.Lines.Add(
  ''#13#10+
  'interface'#13#10+
  ''#13#10+
  '{$SCOPEDENUMS ON}'#13#10+
  ''#13#10+
  'uses'#13#10+
  '  System.Types, System.Classes, FGX.Forms, FGX.Forms.Types, FGX.Controls, FGX.Controls.Types,'#13#10+
  '  '+SourceModuleName+';'#13#10+
  ''#13#10+
  'type'#13#10);
  mPas.Lines.Add( format('  %s = class(%s)', [eClassname.Text, SourceClassName]));
  mPas.Lines.Add(
  '  public'#13#10+
  '  end;'#13#10+
  ''#13#10+
  'implementation'#13#10+
  ''#13#10+
  '{$R *.xfm}'#13#10+
  ''#13#10+
  'end.'#13#10);
end;

procedure TForm1.drop1Click(Sender: TObject);
begin
  if dlgOpen1.Execute then
  begin
    if ExtractFileExt(dlgOpen1.FileName)='.dpr' then
      LoadProject(dlgOpen1.FileName);
  end;
end;

procedure TForm1.drop1Dropped(Sender: TObject; const Data: TDragObject;
  const Point: TPointF);
var i:integer;
begin
  for i:=0 to High(Data.Files) do
  begin
    if ExtractFileExt(data.Files[i])='.dpr' then
    begin
      loadproject(data.Files[i]);
      exit;
    end;
  end;
end;


procedure TForm1.eClassnameChangeTracking(Sender: TObject);
var
  arr:TArray<string>;
  cl,s:string;
  path:string;
  i: Integer;
begin
  arr := eClassname.Text.Split(['\']);
  path := '';
  for i := 0 to High(arr)-1 do
  begin
    path := path + arr[i] + '\';
  end;
  cl := eClassname.Text;
  if cl.StartsWith('T') then
  begin
    cl := cl.Substring(1);
    s := 'U'+cl+'.pas';
    eFilename.Text := path + s;
  end;
end;

end.
